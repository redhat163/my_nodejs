//var express = require('express');
//var router = express.Router();

/* GET users listing. */
//router.get('/', function(req, res, next) {
//  res.send('respond with a resource');
//});

//module.exports = router;


var User = require('../models/User');
var express = require('express');
var router = express.Router();

router.route('/users')
.get(function(req, res) {
	User.find(function(err, users) {
    if (err) {
      return res.send(err);
    }

    res.json(users);
  });
})
.post(function(req, res) {
  var user = new User(req.body);

  user.save(function(err) {
    if (err) {
      return res.send(err);
    }

    res.send({ message: 'User Added' });
  });
});


router.route('/users/:id').put(function(req,res){
	//alert(req.params.id);
	User.findOne({ _id: req.params.id }, function(err, user) {
	    if (err) {
	      return res.send(err);
	    }
	 
	    for (prop in req.body) {
	      user[prop] = req.body[prop];
	    }
	 
	    // save the user
	    user.save(function(err) {
	      if (err) {
	        return res.send(err);
	      }
	 
	      res.json({ message: 'User updated!' });
	    });
	  });
	});

router.route('/users/:id').get(function(req, res) {
	  User.findOne({ _id: req.params.id}, function(err, user) {
	    if (err) {
	      return res.send(err);
	    }
	 
	    res.json(user);
	  });
	});

/*router.route('/movies/:id').delete(function(req, res) {
	  User.remove({
	    _id: req.params.id
	  }, function(err, user) {
	    if (err) {
	      return res.send(err);
	    }
	 
	    res.json({ message: 'Successfully deleted' });
	  });
	});*/

module.exports = router;
