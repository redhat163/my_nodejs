Here are some points about the above directory structure:

The bin/www.js is used to bootstrap our app.
The models directory stores our mongoose models. For this app we will have just one file called user.js now.
The routes directory will store all the Express routes.
The app.js holds the configurations for our Express app.
Finally, node_modules and package.json are the usual components of a Node.js app.