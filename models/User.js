var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var ObjectId = Schema.ObjectId;
 
//disable the "__v" attribute in your Schema definitions by setting the versionKey option to false.
var userSchema = new Schema({
  _id: String,
  name: String,
  pw: String,
  email: String
}, { versionKey: false });

module.exports = mongoose.model('User', userSchema);